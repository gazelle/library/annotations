package net.ihe.gazelle.lib.annotation.test;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static net.ihe.gazelle.lib.annotations.CSVFileLoader.readCSVFile;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.fail;

public class CoversImplementationIT {

    private static final String ASSERTION_ID_COLUMN_NAME = "Assertion_ID";
    private static final String IMPLEMENTED_COLUMN_NAME = "Implemented";
    private static final String TESTED_COLUMN_NAME = "Tested";
    private static final String DEFAULT_SEPARATOR = ",";

    /**
     * test creation of assertion file without preexisting file
     * @throws IOException if file not found
     */
    @Test
    @Covers(requirements = {"COV-3", "COV-4", "COV-5", "COV-6", "COV-7", "COV-8", "COV-9", "COV-10", "COV-11"})
    public void coversImplementationNonExistingFileTest() throws IOException {
        String csvFilePath = "./src/test/resources/nonExisting-filled.csv";

        File csvOutputFile = new File(csvFilePath);
        assertTrue(csvOutputFile.exists(), "output csv file should exist");
        verifyColumnValueForAssertionID(csvOutputFile,IMPLEMENTED_COLUMN_NAME,"test");
        verifyColumnValueForAssertionID(csvOutputFile,TESTED_COLUMN_NAME,"test");
        verifyColumnValueForAssertionID(csvOutputFile,IMPLEMENTED_COLUMN_NAME,"implementation");
        Files.delete(csvOutputFile.toPath());
    }

    /**
     * test creation of assertion file with preexisting file
     * @throws IOException if file not found
     */
    @Test
    @Covers(requirements = {"COV-3", "COV-4", "COV-5", "COV-6", "COV-7", "COV-8", "COV-9", "COV-10", "COV-11"})
    public void coversImplementationTest() throws IOException {

        String csvFilePath = "./src/test/resources/covers-filled.csv";

        File csvOutputFile = new File(csvFilePath);
        assertTrue(csvOutputFile.exists(), "output csv file should exist");
        verifyColumnValueForAssertionID(csvOutputFile,IMPLEMENTED_COLUMN_NAME,"test");
        verifyColumnValueForAssertionID(csvOutputFile,TESTED_COLUMN_NAME,"test");
        verifyColumnValueForAssertionID(csvOutputFile,IMPLEMENTED_COLUMN_NAME,"implementation");
        Files.delete(csvOutputFile.toPath());
    }


    /**
     * verify column value for assertion id
     * @param filledCSVFile the file to check
     * @param columnName the name of the column to check
     * @param assertionID the assertion id
     */
    private void verifyColumnValueForAssertionID(File filledCSVFile, String columnName, String assertionID) throws IOException {
        try {
            Integer columnIndex = null;
            Integer assertionIDIndex = null;
            List<String> filledCSVFileContent = readCSVFile(filledCSVFile);
            String [] columnsTable = filledCSVFileContent.get(0).split(DEFAULT_SEPARATOR);
            for (int i=0;i<columnsTable.length;i++) {
                if (columnsTable[i].equals(columnName)) {
                    columnIndex = i;
                }
            }
            for (int j=0;j<filledCSVFileContent.size();j++) {
                if (filledCSVFileContent.get(j).contains(assertionID)) {
                    assertionIDIndex = j;
                }
            }
            if (assertionIDIndex == null) {
                fail("AssertionID or column name not found");
            } else {
                assertEquals("true",filledCSVFileContent.get(assertionIDIndex).split(DEFAULT_SEPARATOR,-1)[columnIndex],columnName + " value " +
                        "for " + assertionID + " must be true");
            }

        } catch (FileNotFoundException e) {
            fail("File not found : it must be existing");
        }
    }

}
