package net.ihe.gazelle.lib.annotation.test;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.*;

class CoversImplementationTest {

    @Test
    @Covers(requirements = "test")
    void coversTest() throws IOException {
        String csvFilePath = "./src/test/resources/nonExisting-filled.csv";
        File csvOutputFile = new File(csvFilePath);
        if(csvOutputFile.exists()) {
            Files.delete(csvOutputFile.toPath());
        }
        assertTrue(true);
    }

    @Test
    @Covers(requirements = "test")
    void coversTest2() throws IOException {
        String csvFilePath = "./src/test/resources/covers-filled.csv";
        File csvOutputFile = new File(csvFilePath);
        if(csvOutputFile.exists()) {
            Files.delete(csvOutputFile.toPath());
        }
        assertTrue(true);
    }
}