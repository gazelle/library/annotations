package net.ihe.gazelle.lib.annotation.test;

import net.ihe.gazelle.lib.annotations.Covers;

public class CoversImplementation {

    @Covers(requirements = "implementation")
    public void coversImplementation() {
        // Method to test implementation coverage
    }
}
