# Annotations

## @Package

Source annotation used to declare explicitly that a class, method or field has a visibility
 __package__. This helps code reviewers to be sure the intention of the developper was to set
  the visibility to __package__ and not a typo.
  
```java
import net.ihe.gazelle.lib.annotations.Package;

@Package
class MyClass {

   public MyClass() {
      //...   
   }

   // Not compliant
   computeInfo() {
      //...
   }

   // Compliant
   @Package
   computeAnotherInfo() {
      //...
   }

}
```

## @Covers

### Annotation

Annotation used to define which requirement is covered by the annotated method or class. Usage on
 Test classes is highly recommended.
 
 ```java
 import net.ihe.gazelle.lib.annotations.Covers;
 
 @Covers(requirement = {"USER-1", "USER-2"})
 public class MyClassTest {
 
    @Covers(requirements = {"SYSTEM-8", "SYSTEM-9"})
    public void testUserImplementationCase1() {
       //...
    }

    @Covers(requirements = {"SYSTEM-10"})
    public void testUserImplementationCase1() {
       //...
    }
 
 }
 ```

### Coverage generation

In order to generate the specification coverage of the application, a process will generate a csv with the information of whether the assertion is tested or implemented.
To do so, the following plugin will be added to the `pom.xml` of the client application.
  ```xml
     <plugin>
             <groupId>org.bsc.maven</groupId>
             <artifactId>maven-processor-plugin</artifactId>
             <version>4.0-beta1</version>
             <executions>
                 <execution>
                     <id>generateCoverage</id>
                     <goals>
                         <goal>process</goal>
                         <goal>process-test</goal>
                     </goals>
                     <phase>compile</phase>
                     <configuration>
                         <processors>
                             <processor>net.ihe.gazelle.lib.annotations.CoversProcessor</processor>
                         </processors>
                         <systemProperties>
                             <csv.name>{{the original csv path}}</csv.name>
                         </systemProperties>
                     </configuration>
                 </execution>
             </executions>
         </plugin>
 ```
 
 ## Dependency
 
 To use those annotations, add the dependency to the library in the `pom.xml` of the client application.
 
 ```xml
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>lib.annotations</artifactId>
        <version>...</version>
    </dependency>
```

In order to generate the coverage, the following dependency must also be added.
 ```xml
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>annotations-processors</artifactId>
        <version>...</version>
    </dependency>
```