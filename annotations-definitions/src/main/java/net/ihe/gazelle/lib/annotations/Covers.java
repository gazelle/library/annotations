package net.ihe.gazelle.lib.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to define which requirement is covered by the annotated method or class. Usage on Test classes is highly recommended.
 */
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Covers {
   public String[] requirements() default "";
}
