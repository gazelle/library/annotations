package net.ihe.gazelle.lib.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Source annotation used to declare explicitly that a class, method or field has a visibility package. This helps code reviewers to be sure the
 * intention of the developper was to set the visibility to package and not a typo.
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.CONSTRUCTOR})
public @interface Package {
}
