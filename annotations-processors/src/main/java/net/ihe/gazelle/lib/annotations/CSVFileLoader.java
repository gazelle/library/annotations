package net.ihe.gazelle.lib.annotations;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utilitary class to deal with csv file
 */
@Package
public
class CSVFileLoader {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(CSVFileLoader.class);
    private static final String DEFAULT_SEPARATOR = ",";
    private static final String ASSERTION_ID_COLUMN_NAME = "Assertion_ID";
    private static final String IMPLEMENTED_COLUMN_NAME = "Implemented";
    private static final String TESTED_COLUMN_NAME = "Tested";

    private CSVFileLoader(){}

    /**
     * load csv file and create copy to fill
     * @param csvFilePath the path of the csv file
     */
    @Package
    static void loadCSVFileFromPath(String csvFilePath) throws IOException {
        File csvBaseFile = new File(csvFilePath);
            String csvFilledFilePath = csvFilePath.replace(".csv","-filled.csv");
            File csvFilledFile = new File(csvFilledFilePath);
            if (!csvFilledFile.exists() && csvBaseFile.exists() && checkAssertionIdColumnsExistsInBaseFile(csvBaseFile)) {
                LOGGER.info("File already exists.");
                copyCSVBaseFileIntoFilledFile(csvBaseFile,csvFilledFile);
            } else if (!csvFilledFile.exists()) {
                if (csvFilledFile.createNewFile()) {
                    LOGGER.info("Filled File created: " + csvFilledFile.getName());
                } else {
                    LOGGER.error("Could not create the filled file");
                }
            }
            verifyIfMissingColumnNameInCSVFile(csvFilledFile,ASSERTION_ID_COLUMN_NAME);
            verifyIfMissingColumnNameInCSVFile(csvFilledFile,IMPLEMENTED_COLUMN_NAME);
            verifyIfMissingColumnNameInCSVFile(csvFilledFile,TESTED_COLUMN_NAME);
    }

    /**
     * check presence of the assertion Id column in file
     * @param csvBaseFile the file to search
     * @return true if the column exists
     */
    private static boolean checkAssertionIdColumnsExistsInBaseFile(File csvBaseFile) throws IOException {
            List<String> csvBaseFileContent = readCSVFile(csvBaseFile);
            if (!csvBaseFileContent.isEmpty()) {
                return csvBaseFileContent.get(0).contains(ASSERTION_ID_COLUMN_NAME);
            } else {
                return false;
            }
    }

    /**
     * copy the content of the base file into a new file
     * @param csvBaseFile the base file
     * @param csvFilledFile the new file to fill
     * @throws IOException if the file is not found
     */
    private static void copyCSVBaseFileIntoFilledFile(File csvBaseFile, File csvFilledFile) throws IOException {
        OutputStream outputStream = new FileOutputStream(csvFilledFile);
        Files.copy(csvBaseFile.toPath(),outputStream);
        outputStream.close();
    }

    /**
     * read the csv file
     * @param csvFile the file to read
     * @return the csv lines
     * @throws FileNotFoundException if the file is not found
     */
    @Package
    public static List<String> readCSVFile(File csvFile) throws IOException {
        List<String> result = new ArrayList<>();
        FileReader fr = new FileReader(csvFile);

        try (BufferedReader br = new BufferedReader(fr)) {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                result.add(line);
            }
            fr.close();
        }


        return result;
    }

    /**
     * set implemented to true for an assertion
     * @param csvFilePath the csv to modify
     * @param assertionID the assertion id
     */
    @Package
    static void setImplementedForAssertionID(String csvFilePath, String assertionID) throws IOException {
        setColumnNameValueForAssertionID(csvFilePath,IMPLEMENTED_COLUMN_NAME,assertionID);
    }

    /**
     * set testes to true for an assertion
     * @param csvFilePath the csv to modify
     * @param assertionID the assertion id
     */
    @Package
    static void setTestedForAssertionID(String csvFilePath, String assertionID) throws IOException {
        setColumnNameValueForAssertionID(csvFilePath,TESTED_COLUMN_NAME,assertionID);
    }

    /**
     * set column to true for an assertion
     * @param csvFilePath the csv to modify
     * @param columnName the name to set to true
     * @param assertionID the assertion id
     */
    private static void setColumnNameValueForAssertionID(String csvFilePath, String columnName, String assertionID) throws IOException {
            int columnIndex = 0;
            Integer assertionIDIndex = null;
            int assertionIDColumnIndex = 0;
            File csvFilledFile = new File(csvFilePath.replace(".csv","-filled.csv"));
            List<String> filledCSVFileContent = readCSVFile(csvFilledFile);
            String [] columnsTable = filledCSVFileContent.get(0).split(DEFAULT_SEPARATOR);
            for (int i=0;i<columnsTable.length;i++) {
                if (columnsTable[i].equals(columnName)) {
                    columnIndex = i;
                } else if (columnsTable[i].equals(ASSERTION_ID_COLUMN_NAME)) {
                    assertionIDColumnIndex = i;
                }
            }
            for (int j=0;j<filledCSVFileContent.size();j++) {
                if (filledCSVFileContent.get(j).contains(assertionID + DEFAULT_SEPARATOR)) {
                    assertionIDIndex = j;
                }
            }
            if (assertionIDIndex != null) {
                String [] assertionIdLineTable = filledCSVFileContent.get(assertionIDIndex).split(DEFAULT_SEPARATOR,-1);
                assertionIdLineTable[columnIndex] = "true";
                filledCSVFileContent.set(assertionIDIndex,String.join(DEFAULT_SEPARATOR,assertionIdLineTable));
            } else {
                String [] newAssertionIdLineTable = new String[columnsTable.length];
                Arrays.fill(newAssertionIdLineTable, "");
                newAssertionIdLineTable[assertionIDColumnIndex] = assertionID;
                newAssertionIdLineTable[columnIndex] = "true";
                filledCSVFileContent.add(String.join(DEFAULT_SEPARATOR,newAssertionIdLineTable));
            }
            writeInFile(csvFilledFile,filledCSVFileContent);
    }

    /**
     * create column if missing
     * @param csvFile the file to check
     * @param missingColumnName the column to check
     * @throws IOException if file is not accessible
     */
    private static void verifyIfMissingColumnNameInCSVFile(File csvFile, String missingColumnName) throws IOException {
        List<String> csvFileContent = readCSVFile(csvFile);
        if (!csvFileContent.isEmpty()) {
            if (!csvFileContent.get(0).contains(missingColumnName)) {
                for (int i=0 ; i<csvFileContent.size();i++) {
                    csvFileContent.set(i,csvFileContent.get(i).concat(DEFAULT_SEPARATOR));
                }
                csvFileContent.set(0,csvFileContent.get(0).concat(missingColumnName));
                writeInFile(csvFile,csvFileContent);
            }
        } else {
            csvFileContent.add(missingColumnName);
            writeInFile(csvFile,csvFileContent);
        }
    }

    /**
     * write csv content to file
     * @param csvFile the file to write
     * @param contentToWriteList the lines to write
     * @throws IOException if file is not accessible
     */
    private static void writeInFile(File csvFile, List<String> contentToWriteList) throws IOException {
        try (FileWriter fileWriter = new FileWriter(csvFile)) {
            for (String content : contentToWriteList) {
                fileWriter.write(content + "\n");
            }
        }
    }
}
