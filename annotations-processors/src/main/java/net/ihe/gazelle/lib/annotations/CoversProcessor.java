package net.ihe.gazelle.lib.annotations;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

@SupportedAnnotationTypes(
        "net.ihe.gazelle.lib.annotations.Covers")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
public class CoversProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations,
                           RoundEnvironment roundEnv) {
        String fileName = System.getProperty("csv.name");
        AtomicBoolean isTest = new AtomicBoolean(false);
        roundEnv.getRootElements().iterator().forEachRemaining(element ->  {
            String className = element.getSimpleName().toString();
            isTest.set(className.endsWith("Test") || className.endsWith("IT") || isTest.get());
        });
        final boolean isTestFinal = isTest.get();

        for (TypeElement annotation : annotations) {
            try {
                CSVFileLoader.loadCSVFileFromPath(fileName);
                roundEnv.getElementsAnnotatedWith(annotation).forEach(annotatedElement -> Arrays.asList(annotatedElement.getAnnotation(Covers.class).requirements()).forEach(assertionId -> {
                    try {
                    if (isTestFinal) {
                        CSVFileLoader.setTestedForAssertionID(fileName, assertionId);
                    }
                    CSVFileLoader.setImplementedForAssertionID(fileName, assertionId);
                    } catch (IOException ignored) {
                }
                }));
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }
}