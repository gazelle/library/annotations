package net.ihe.gazelle.lib.annotations;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static net.ihe.gazelle.lib.annotations.CSVFileLoader.*;
import static org.junit.jupiter.api.Assertions.*;

public class CSVFileLoaderTest {

    private static final String ASSERTION_ID_COLUMN_NAME = "Assertion_ID";
    private static final String IMPLEMENTED_COLUMN_NAME = "Implemented";
    private static final String TESTED_COLUMN_NAME = "Tested";
    private static final String DEFAULT_SEPARATOR = ",";


    /**
     * test file generation with existing file
     */
    @Test
    public void testLoadCSVExistingFile() throws IOException {
        String csvFilePath = "./src/test/resources/allColumns.csv";
        String csvFileFilledPath = "./src/test/resources/allColumns-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        assertTrue(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must exist");
        csvFilledFile.delete();
    }

    /**
     * test file generation with non existing file
     */
    @Test
    public void testLoadCSVWithNonExistingFile() throws IOException {
        String csvFilePath = "./src/test/resources/nonExistingFile.csv";
        String csvFileFilledPath = "./src/test/resources/nonExistingFile-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        assertTrue(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must exist");
        csvFilledFile.delete();
    }

    /**
     * test file column generation with non existing file
     */
    @Test
    public void testAllColumnsGeneratedBaseCSVNonExisting() throws IOException {
        String csvFilePath = "./src/test/resources/nonExistingFile.csv";
        String csvFileFilledPath = "./src/test/resources/nonExistingFile-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        verifyIfAllColumnsExist(csvFilledFile);
        csvFilledFile.delete();
    }

    /**
     * test read generated csv
     */
    @Test
    public void testReadCSVFileWithContent() throws IOException {
        String csvFilePath = "./src/test/resources/allColumns.csv";
        File csvFile = new File(csvFilePath);
        List<String> csvFileContent = readCSVFile(csvFile);
        assertNotNull(csvFileContent,"CSVFile must have content");
    }

    /**
     * test read generated csv
     */
    @Test
    public void testReadCSVWithNonExisting() {
        String nonExistingCsvFilePath = "./src/test/resources/unknownFile.csv";
        File unknownCSVFile = new File(nonExistingCsvFilePath);
        assertThrows(FileNotFoundException.class,() -> readCSVFile(unknownCSVFile));
    }

    /**
     * test file column generation with existing file
     */
    @Test
    public void testAllColumnsGeneratedBaseCSVWithAllColumns() throws IOException {
        String csvFilePath = "./src/test/resources/allColumns.csv";
        String csvFileFilledPath = "./src/test/resources/allColumns-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        verifyIfAllColumnsExist(csvFilledFile);
        csvFilledFile.delete();
    }

    /**
     * test file column generation with existing file, assertion id column missing
     */
    @Test
    public void testAllColumnsGeneratedBaseCSVWithoutAssertionID() throws IOException {
        String csvFilePath = "./src/test/resources/withoutAssertionID.csv";
        String csvFileFilledPath = "./src/test/resources/withoutAssertionID-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        verifyIfAllColumnsExist(csvFilledFile);
        csvFilledFile.delete();
    }

    /**
     * test file column generation with existing file, assertion id column only
     */
    @Test
    public void testAllColumnsGeneratedBaseCSVWithOnlyAssertionID() throws IOException {
        String csvFilePath = "./src/test/resources/onlyAssertionID.csv";
        String csvFileFilledPath = "./src/test/resources/onlyAssertionID-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        verifyIfAllColumnsExist(csvFilledFile);
        csvFilledFile.delete();
    }

    /**
     * verify all column are present
     * @param filledCSVFile the file to check
     */
    private void verifyIfAllColumnsExist(File filledCSVFile) throws IOException {
        try {
            List<String> filledCSVFileContent = readCSVFile(filledCSVFile);
            if (!filledCSVFileContent.isEmpty()) {
                assertTrue(filledCSVFileContent.get(0).contains(ASSERTION_ID_COLUMN_NAME));
                assertTrue(filledCSVFileContent.get(0).contains(IMPLEMENTED_COLUMN_NAME));
                assertTrue(filledCSVFileContent.get(0).contains(TESTED_COLUMN_NAME));
            } else {
                fail("The file content is empty");
            }
        } catch (FileNotFoundException e) {
            fail("File not found : it must be existing");
        }
    }

    /**
     * test set implemented, known assertion id
     */
    @Test
    public void testSetImplementedForKnownAssertionID() throws IOException {
        String assertionID = "COV-1";
        String csvFilePath = "./src/test/resources/testSetter.csv";
        String csvFileFilledPath = "./src/test/resources/testSetter-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        setImplementedForAssertionID(csvFilePath,assertionID);
        verifyColumnValueForAssertionID(csvFilledFile,IMPLEMENTED_COLUMN_NAME,assertionID);
        csvFilledFile.delete();
    }

    /**
     * test set tested, known assertion id
     */
    @Test
    public void testSetTestedForKnownAssertionID() throws IOException {
        String assertionID = "COV-1";
        String csvFilePath = "./src/test/resources/testSetter.csv";
        String csvFileFilledPath = "./src/test/resources/testSetter-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        setTestedForAssertionID(csvFilePath,assertionID);
        verifyColumnValueForAssertionID(csvFilledFile,TESTED_COLUMN_NAME,assertionID);
        csvFilledFile.delete();
    }

    /**
     * test set implemented, unknown assertion id
     */
    @Test
    public void testSetImplementedForUnKnownAssertionID() throws IOException {
        String assertionID = "COV-3";
        String csvFilePath = "./src/test/resources/testSetter.csv";
        String csvFileFilledPath = "./src/test/resources/testSetter-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        setImplementedForAssertionID(csvFilePath,assertionID);
        verifyColumnValueForAssertionID(csvFilledFile,IMPLEMENTED_COLUMN_NAME,assertionID);
        csvFilledFile.delete();
    }

    /**
     * test set tested, unknown assertion id
     */
    @Test
    public void testSetTestedForUnKnownAssertionID() throws IOException {
        String assertionID = "COV-3";
        String csvFilePath = "./src/test/resources/testSetter.csv";
        String csvFileFilledPath = "./src/test/resources/testSetter-filled.csv";
        File csvFilledFile = new File(csvFileFilledPath);
        if (csvFilledFile.exists()) {
            csvFilledFile.delete();
        }
        assertFalse(Files.exists(Paths.get(csvFileFilledPath)),"CSVFilledFile must not exist");
        loadCSVFileFromPath(csvFilePath);
        setTestedForAssertionID(csvFilePath,assertionID);
        verifyColumnValueForAssertionID(csvFilledFile,TESTED_COLUMN_NAME,assertionID);
        csvFilledFile.delete();
    }

    /**
     * verify column value for assertion id
     * @param filledCSVFile the file to check
     * @param columnName the name of the column to check
     * @param assertionID the assertion id
     */
    private void verifyColumnValueForAssertionID(File filledCSVFile, String columnName, String assertionID) throws IOException {
        try {
            Integer columnIndex = null;
            Integer assertionIDIndex = null;
            List<String> filledCSVFileContent = readCSVFile(filledCSVFile);
            String [] columnsTable = filledCSVFileContent.get(0).split(DEFAULT_SEPARATOR);
            for (int i=0;i<columnsTable.length;i++) {
               if (columnsTable[i].equals(columnName)) {
                   columnIndex = i;
               }
            }
            for (int j=0;j<filledCSVFileContent.size();j++) {
                if (filledCSVFileContent.get(j).contains(assertionID)) {
                    assertionIDIndex = j;
                }
            }
            if (assertionIDIndex == null) {
                fail("AssertionID or column name not found");
            } else {
                assertEquals("true",filledCSVFileContent.get(assertionIDIndex).split(DEFAULT_SEPARATOR,-1)[columnIndex],columnName + " value " +
                        "for " + assertionID + " must be true");
            }

        } catch (FileNotFoundException e) {
            fail("File not found : it must be existing");
        }
    }

}
